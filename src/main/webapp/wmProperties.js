var _WM_APP_PROPERTIES = {
  "activeTheme" : "material",
  "dateFormat" : "",
  "defaultLanguage" : "hi",
  "displayName" : "AppVariables",
  "homePage" : "Main",
  "name" : "AppVariables",
  "platformType" : "WEB",
  "securityEnabled" : "true",
  "supportedLanguages" : "en,hi-in,hi",
  "timeFormat" : "",
  "type" : "APPLICATION",
  "version" : "1.0"
};