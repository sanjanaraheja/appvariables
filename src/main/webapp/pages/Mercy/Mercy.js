Application.$controller("MercyPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

}]);




Application.$controller("dialog1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.DepartmentTable1Select = function($event, $isolateScope, $rowData) {
            debugger;
            $scope.Variables.staticVariable1.dataSet.push($rowData);
        };


        $scope.dialog1Opened = function($event, $isolateScope) {
            // debugger;
            // $scope.Widgets.DepartmentTable1.selectItem($scope.Variables.staticVariable1.dataSet);
        };


        $scope.DepartmentTable1Datarender = function($isolateScope, $data) {
            debugger;
            $scope.Widgets.DepartmentTable1.selecteditem = $scope.Variables.staticVariable1.dataSet;
        };

    }
]);

Application.$controller("staticVariable1Table1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("DepartmentTable1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);