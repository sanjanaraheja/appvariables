Application.$controller("ChangePageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };


    $scope.text1Change = function($event, $isolateScope, newVal, oldVal) {
        debugger;
        alert("Fired");
    };


    $scope.button1Click = function($event, $isolateScope) {
        debugger;
        var textScope = $scope.Widgets.text1;
        var oldVal = textScope.datavalue;
        var newVal = "test"
        textScope.datavalue = newVal;
        $scope.text1Change(null, textScope, newVal, oldVal);
    };

}]);