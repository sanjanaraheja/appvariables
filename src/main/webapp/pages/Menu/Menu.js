Application.$controller("MenuPageController", ["$scope", "$timeout", function($scope, $timeout) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

    $scope.$root.$on('locale-change', function() {
        $scope.Widgets.menu1.dataset = "";
        $timeout(function() {
            $scope.Widgets.menu1.dataset = $scope.Variables.menudata.dataSet;
        });
    });
}]);