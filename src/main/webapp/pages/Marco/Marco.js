var nextPage;
Application.$controller("MarcoPageController", ["$scope", "$window", "wmSpinner", "DialogService", function($scope, $window, wmSpinner, DialogService) {
    "use strict";
    $scope.$on('$locationChangeStart', function(event, next, current) {
        if ($scope.Widgets.UserLiveForm1.ngform.$dirty) {
            if (nextPage) {
                nextPage = undefined;
                return;
            }
            wmSpinner.hide('globalSpinner')
            nextPage = next;

            DialogService.open('confirmdialog1', $scope, {

            });

            event.preventDefault();
        }

    });


    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

}]);


Application.$controller("UserLiveForm1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);


Application.$controller("confirmdialog1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.confirmdialog1Ok = function($event, $isolateScope) {
            window.location.href = nextPage;
        };


        $scope.confirmdialog1Cancel = function($event, $isolateScope) {
            nextPage = undefined;
        };

    }
]);