Application.$controller("ValidatontestPageController", ["$scope", "$rootScope", function($scope, $rootScope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };


    $scope.checkbox1Change = function($event, $isolateScope, newVal, oldVal) {
        if (newVal) {
            //$scope.Widgets.username_formWidget.regexp = "^WFH";
            $scope.Widgets.text1.regexp = "^WFH";
        } else {
            $scope.Widgets.text1.regexp = "^(?!WFH.*$).*";
        }
    };


    $scope.button1Click = function($event, $isolateScope) {
        // if(data.<field_name> === "No Code Found"|| $scope.Widgets.label1.caption === "No Code Found"){
        // 			    $scope.Widgets.addClass('ng-touched');
        // 			}
        debugger;
        if ($scope.Widgets.label2.caption === "No Code Found") {
            $scope.Widgets.text1.$element.addClass('ng-invalid');
        }
    };


    $scope.button2Click = function($event, $isolateScope) {
        debugger;
        $scope.Variables.HrdbUserData.setFilter('userId', $rootScope.testInput($scope.Widgets.text1.datavalue));
        $scope.Variables.HrdbUserData.update();
    };


    $scope.toggle1Change = function($event, $isolateScope, newVal, oldVal) {
        if (newVal) {
            $scope.Widgets.text1.regexp = "^WFH";
        } else {
            $scope.Widgets.text1.regexp = "^(?!WFH.*$).*";
        }
    };

}]);


Application.$controller("UserLiveForm1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);