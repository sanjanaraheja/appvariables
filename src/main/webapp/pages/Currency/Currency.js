Application.$controller("CurrencyPageController", ["$scope", "$timeout", function($scope, $timeout) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        debugger;
        var ngModelCtrl = $scope.Widgets.currency1.$element.find('input').controller('ngModel');
        ngModelCtrl.$formatters.push(function(value) {
            console.log(value);
            return parseFloat(value).toFixed(2);
        });
    };




    // $scope.currency1Focus = function($event, $isolateScope) {
    //     parseFloat($isolateScope.datavalue).toFixed(2);
    // };

}]);