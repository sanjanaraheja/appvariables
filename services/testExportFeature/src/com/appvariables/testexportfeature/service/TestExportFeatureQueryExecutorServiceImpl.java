/*Generated by WaveMaker Studio*/

package com.appvariables.testexportfeature.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wavemaker.runtime.data.dao.query.WMQueryExecutor;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.file.model.Downloadable;

import com.appvariables.testexportfeature.models.query.*;

@Service
public class TestExportFeatureQueryExecutorServiceImpl implements TestExportFeatureQueryExecutorService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestExportFeatureQueryExecutorServiceImpl.class);

    @Autowired
    @Qualifier("testExportFeatureWMQueryExecutor")
    private WMQueryExecutor queryExecutor;

    @Transactional(readOnly = true, value = "testExportFeatureTransactionManager")
    @Override
    public Page<DocumentListResponse> executeDocumentList(Integer id, Pageable pageable) {
        Map params = new HashMap(1);

        params.put("id", id);

        return queryExecutor.executeNamedQuery("DocumentList", params, DocumentListResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "testExportFeatureTransactionManager")
    @Override
    public Downloadable exportDocumentList(ExportType exportType, Integer id, Pageable pageable) {
        Map params = new HashMap(1);

        params.put("id", id);

        return queryExecutor.exportNamedQueryData("DocumentList", params, exportType, DocumentListResponse.class, pageable);
    }

    @Transactional(value = "testExportFeatureTransactionManager")
    @Override
    public Integer executeDataGridAction(DataGridActionRequest dataGridActionRequest) {
        Map params = new HashMap(4);

        params.put("id", dataGridActionRequest.getId());
        params.put("name", dataGridActionRequest.getName());
        params.put("dept", dataGridActionRequest.getDept());
        params.put("admin", dataGridActionRequest.getAdmin());

        return queryExecutor.executeNamedQueryForUpdate("DataGridAction", params);
    }

    @Transactional(readOnly = true, value = "testExportFeatureTransactionManager")
    @Override
    public Page<TestMercyResponse> executeTestMercy(Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.executeNamedQuery("testMercy", params, TestMercyResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "testExportFeatureTransactionManager")
    @Override
    public Downloadable exportTestMercy(ExportType exportType, Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.exportNamedQueryData("testMercy", params, exportType, TestMercyResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "testExportFeatureTransactionManager")
    @Override
    public Page<DocClassListResponse> executeDocClassList(Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.executeNamedQuery("DocClassList", params, DocClassListResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "testExportFeatureTransactionManager")
    @Override
    public Downloadable exportDocClassList(ExportType exportType, Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.exportNamedQueryData("DocClassList", params, exportType, DocClassListResponse.class, pageable);
    }

}


