/*Generated by WaveMaker Studio*/

package com.hrdb.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wavemaker.runtime.data.dao.query.WMQueryExecutor;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.file.model.Downloadable;

import com.hrdb.models.query.*;

@Service
public class HrdbQueryExecutorServiceImpl implements HrdbQueryExecutorService {

    private static final Logger LOGGER = LoggerFactory.getLogger(HrdbQueryExecutorServiceImpl.class);

    @Autowired
    @Qualifier("hrdbWMQueryExecutor")
    private WMQueryExecutor queryExecutor;

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Page<TestAppVariableResponse> executeTestAppVariable(Date startDate, String status, Pageable pageable) {
        Map params = new HashMap(2);

        params.put("startDate", startDate);
        params.put("status", status);

        return queryExecutor.executeNamedQuery("testAppVariable", params, TestAppVariableResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Downloadable exportTestAppVariable(ExportType exportType, Date startDate, String status, Pageable pageable) {
        Map params = new HashMap(2);

        params.put("startDate", startDate);
        params.put("status", status);

        return queryExecutor.exportNamedQueryData("testAppVariable", params, exportType, TestAppVariableResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Page<I3Response> executeI3(Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.executeNamedQuery("i3", params, I3Response.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Downloadable exportI3(ExportType exportType, Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.exportNamedQueryData("i3", params, exportType, I3Response.class, pageable);
    }

}


